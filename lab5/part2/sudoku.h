//Ryan Mackey
//Sudoku class- templated
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

template< typename T >
class sudoku{
public:
	sudoku();
	void print();
	void play();
private:
	vector< vector< T > > puzzle;
};

//Tamplated constructor- fills a 2D vector with a sudoku puzzle form another 
//file
template<typename T >
sudoku<T>::sudoku(){
	vector<T> extraVec;
	T value;
	ifstream infile;
	if(sizeof(T) == sizeof(int)){
		infile.open("sudoku.txt");
	}else if (sizeof(T) == sizeof(char)){
		infile.open("wordoku.txt");
	}
	for(int i=0; i<9; i++){
		for(int j=0; j<9; j++){
			infile >> value;
			extraVec.push_back(value);
		}
		puzzle.push_back(extraVec);
		extraVec.clear();
	}
}

//play function- lets the user fill in a smart sudoku board
template<typename T >
void sudoku<T>::play(){
	T guess; // depends on templating
	int fail=0, unfinished=1, r, c;
	while(unfinished){
		cout << "Row? ";
		cin >> r;// row to fill
		r--;
		cout << "Column? ";
		cin >> c; // column to fill
		c--;
		fail=0;
		unfinished=0; // chance to break out of loop
		if(puzzle[r][c]==0){ // checking if open
			cout << "Guess? ";
			cin >> guess;
			for(int i=0; i<9; i++){ // checking up/down and sideways
				if(puzzle[r][i] == guess){//vertical
					fail = 1;//number may not appear twice
				}
				if(puzzle[i][c] == guess){//horizontal
					fail = 1;
				}
			}
			//checking the gridlets 3x3
			for(int i=((c/3)*3); i<(((c/3)*3)+3); i++){
				for(int j=((r/3)*3); j<(((r/3)*3)+3); j++){
					if(puzzle[i][j] == guess){
						fail = 1;
					}
				}
			}
			if(!fail){//adds the number in if appropriate
				puzzle[r][c]=guess;
			}
		}
		else//warning throw
			cout << "Pick an open slot!" << endl;
		if(fail)//throw warning
			cout << "Illegal Move!" << endl;
		print();//reprint board
		//check for any open spots on the board
		for(int i=0; i<9; i++){
			for(int j=0; j<9; j++){
				if(puzzle[i][j] == 0){
					unfinished = 1;//keep playing
				}
			}
		}
	}
	cout << "You did it!!!!!!!!" << endl;//YAY
}

//print the board one block at a time
template<typename T >
void sudoku<T>::print(){
	for(int i=0; i< puzzle.size(); i++){
		for(int j=0; j< puzzle[i].size(); j++){
			cout << puzzle[i][j];
		}
		cout << endl;
	}
	cout << endl;
}
