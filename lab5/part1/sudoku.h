//Ryan Mackey
//templated sudoku class and member functions
//February 24, at 3 am
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

//templated class example
template< typename T >
class sudoku{
public:
	sudoku();//constructor
	void print();//print function
private:
	//2-D vector example
	vector< vector< T > > puzzle;
};

//constructor with file reading
template<typename T >
sudoku<T>::sudoku(){
	vector<T> extraVec;
	T value;

	//pick the proper file to read based on T type
	ifstream infile;
	if(sizeof(T) == sizeof(int)){
		infile.open("sudoku.txt");
	}else if (sizeof(T) == sizeof(char)){
		infile.open("wordoku.txt");
	}
	//fill the 2-D vector
	for(int i=0; i<9; i++){
		for(int j=0; j<9; j++){
			infile >> value;
			extraVec.push_back(value);
		}
		puzzle.push_back(extraVec);
		extraVec.clear();
	}
}

//print the sudoku puzzle one block at a time
template<typename T >
void sudoku<T>::print(){
	for(int i=0; i< puzzle.size(); i++){
		for(int j=0; j< puzzle[i].size(); j++){
			cout << puzzle[i][j]; // print every block
		}
		cout << endl;
	}
	cout << endl;
}
