//Ryan Patrick Mackey
//Sudoku class- templated
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>

using namespace std;

template< typename T >
class sudoku{
public:
	sudoku();
	void print();
	int solve();
	int singleton();
private:
	vector< vector< T > > puzzle;
};

//Templated constructor- fills a 2D vector with a sudoku puzzle form another 
//file
template<typename T >
sudoku<T>::sudoku(){
	vector<T> extraVec;
	T value;
	ifstream infile;
	if(sizeof(T) == sizeof(int)){
		infile.open("me.txt");
	}else if (sizeof(T) == sizeof(char)){
		infile.open("wordoku.txt");
	}
	for(int i=0; i<9; i++){
		for(int j=0; j<9; j++){
			infile >> value;
			extraVec.push_back(value);
		}
		puzzle.push_back(extraVec);
		extraVec.clear();
	}
}

template<typename T >
int sudoku<T>::singleton(){
	int go = 0, r, c;
	vector<vector< int > > group;
	vector< int > checkVec;
	for(int i=0; i<9; i++){
		checkVec.push_back(i+1);
	}
	for(int i=0; i<=9; i++){
		group.push_back(checkVec);
	}
	//row case
	for(int i=0; i<9; i++){
		for(int j=0; j<9; j++){
			if( puzzle[i][j] == 0){
				//fill the possibility matrix
				for(int fill = 0; fill < 9; fill++){
					group[j][fill]=checkVec[fill];
				}
				for(int mov=0; mov<9; mov++){ // place zeros on impossible options
					if(puzzle[i][mov] == group[j][puzzle[i][mov]-1]){
						group[j][puzzle[i][mov]-1] = 0;
					}
					if(puzzle[mov][j] == group[j][puzzle[mov][j]-1]){
						group[j][puzzle[mov][j]-1] = 0;
					}
				}
				for(int g1=((i/3)*3); g1<(((i/3)*3)+3); g1++){
					for(int g2=((j/3)*3); g2<(((j/3)*3)+3); g2++){
						if(puzzle[g1][g2] == group[j][puzzle[g1][g2]-1]){
							group[j][puzzle[g1][g2]-1] = 0;
						}
					}
				}
			}else{
				for(int fill = 0; fill < 9; fill++){// or place a whole row of zeros
					group[j][fill]=0;
				}
			}
		}
		//now the rows are filled with all possibilities- check matrix for Singletons!
		int count = 0, spot = 0;
		for(int col = 0; col<9; col++){
			for(int row =0; row < 9; row++){
				if(group[row][col]==0){
					count++;
					spot = row;
				}
			}
			if(count == 1){ // looking for only one instance of a number int he possibility matrix
				puzzle[i][spot] = col+1; // placing in the puzzle if true.
				go=1;
			}
			count = 0;
		}
	}
	
	//column case
	for(int j=0; j<9; j++){
		for(int i=0; i<9; i++){
			if( puzzle[i][j] == 0){
				//fill the possibility matrix
				for(int fill = 0; fill < 9; fill++){
					group[i][fill]=checkVec[fill];
				}
				for(int mov=0; mov<9; mov++){ // place zeros on impossible options
					if(puzzle[i][mov] == group[i][puzzle[i][mov]-1]){
						group[i][puzzle[i][mov]-1] = 0;
					}
					if(puzzle[mov][j] == group[i][puzzle[mov][j]-1]){
						group[i][puzzle[mov][j]-1] = 0;
					}
				}
				for(int g1=((i/3)*3); g1<(((i/3)*3)+3); g1++){
					for(int g2=((j/3)*3); g2<(((j/3)*3)+3); g2++){
						if(puzzle[g1][g2] == group[i][puzzle[g1][g2]-1]){
							group[i][puzzle[g1][g2]-1] = 0;
						}
					}
				}
			}else{
				for(int fill = 0; fill < 9; fill++){
					group[i][fill]=0;
				} // or place a whole row of zeros
			}
		}
		//now the rows are filled with all possibilities- check matrix for Singletons!
		int count = 0, spot = 0;
		for(int col = 0; col<9; col++){
			for(int row =0; row < 9; row++){
				if(group[row][col]==0){
					count++;
					spot = row;
				}
			}
			if(count == 1){ // looking for only one instance of a number in the possibility matrix
				puzzle[spot][j] = col+1; // placing in the puzzle if true.
				go=1;
			}
			count = 0;
		}
	}

	int f=0;
	//gridlet case
	for( int q=0; q<3; q++){
		for(int a=0; a<3; a++){
			f=0;
			for(int i=(q*3); i<((q*3)+3); i++){
				for(int j=(a*3); j<((a*3)+3); j++){
					f++;
					if( puzzle[i][j] == 0){
						for(int y=0; y<9; y++){
							group[f][y]=checkVec[y];
						}
						for(int mov=0; mov<9; mov++){ // place zeros on impossible options
							if(puzzle[i][mov] == group[j][puzzle[i][mov]-1]){
								group[j][puzzle[i][mov]-1] = 0;
							}
							if(puzzle[mov][j] == group[j][puzzle[mov][j]-1]){
								group[j][puzzle[mov][j]-1] = 0;
							}
						}
						for(int g1=((i/3)*3); g1<(((i/3)*3)+3); g1++){
							for(int g2=((j/3)*3); g2<(((j/3)*3)+3); g2++){
								if(puzzle[g1][g2] == group[j][puzzle[g1][g2]-1]){
									group[j][puzzle[g1][g2]-1] = 0;
								}
							}
						}
					}else{
						for(int fill = 0; fill < 9; fill++){
							group[f][fill]=0;
						} // or place a whole row of zeros
					}
				}
			}
			//now the rows are filled with all possibilities- check matrix for Singletons!
			int count = 0, spot = 0;
			for(int col = 0; col<9; col++){
				for(int row =0; row < 9; row++){
					if(group[row][col]==0){
						count++;
						spot = row;
					}
				}
				if(count == 1){ // looking for only one instance of a number in the possibility matrix
					r=((spot/3)+(q*3));
					c=((spot%3)+(a*3));
					puzzle[r][c] = col+1; // placing in the puzzle if true.
					go=1;
				}
				count = 0;
			}
		}
	}
	print();
	return 0;
}

//brute force solve the puzzle
template<typename T >
int sudoku<T>::solve(){
	vector<int> poss;
	int stop=0, ans=0, again=1, go=0;
	for(int i=0; i<9; i++){
		poss.push_back(i+1);
	}

	while(again){
		again=0;
		for(int i = 0; i<9; i++){
			for(int j=0; j<9; j++){
				if(puzzle[i][j]==0){
					for(int k=0; k<9; k++){
						poss[k] = k+1;
					}
					for(int mov=0; mov<9; mov++){
						if(puzzle[i][mov] == poss[puzzle[i][mov]-1]){
							poss[puzzle[i][mov]-1] = 0;
						}
						if(puzzle[mov][j] == poss[puzzle[mov][j]-1]){
							poss[puzzle[mov][j]-1] = 0;
						}
					}
					for(int g1=((i/3)*3); g1<(((i/3)*3)+3); g1++){
						for(int g2=((j/3)*3); g2<(((j/3)*3)+3); g2++){
							if(puzzle[g1][g2] == poss[puzzle[g1][g2]-1]){
								poss[puzzle[g1][g2]-1] = 0;
							}
						}
					}
					stop =0;
					ans=0;
					for(int k=0; k<9; k++){
						if(poss[k]){
							if(!stop){
								stop=1;
								ans=poss[k];
							}
							else if (stop){
								ans = 0;
							}
						}
					}
					if(ans){
						puzzle[i][j]=ans;
						again=1;
						go=1;
					}
				}
			}
		}
	}
	print();
	return 0;
}

//print the board one block at a time
template<typename T >
void sudoku<T>::print(){
	for(int i=0; i< puzzle.size(); i++){
		for(int j=0; j< puzzle[i].size(); j++){
			cout << puzzle[i][j];
		}
		cout << endl;
	}
	cout << endl;
	return;
}
