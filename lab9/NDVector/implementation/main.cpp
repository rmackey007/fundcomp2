//Ryan Mackey
//March 28, 2016
#include <iostream>
#include "NDVector.h"

using namespace std;


int main() {

	int num;

	NDVector<int> vect1;

	vect1.push_back(1);
	vect1.push_back(2);
	for(int i = 3; i<26; i++){
		vect1.push_back(i);
		num =vect1.back();
		cout << "back: " << num << endl;
		num =vect1.getSize();
		cout << "size: " << num << endl << endl;
	}

	cout << "RAM" << vect1[4] << endl;

	vect1.pop_back();
	vect1.pop_back();
	cout << vect1.back();


	vect1.clear();
	cout <<vect1.getSize() << endl;
	cout << vect1.back();
	return 0;
}
