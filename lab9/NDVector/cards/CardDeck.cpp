//Ryan Mackey
//Member functions for CardDeck
#include <iostream>
#include "CardDeck.h"
#include <deque>
#include <algorithm>

using namespace std;

//redirected ostream operator
ostream& operator<<(ostream& output, CardDeck& deck){
	for (int i = 0; i < deck.cards.getSize()-1; ++i)
		cout << deck.cards[i] << ", ";
	cout << deck.cards.back() << endl;
}

//default constructor
CardDeck::CardDeck(int n){
	int i;
	for(i = 0; i < n; i++){ //numbers 0 through 51
		cards.push_back(i);
	}
}

//return card value
int CardDeck::getCard(int a){
	return cards[a];
}

//size of the deck
int CardDeck::getSize(){
	cards.getSize();
}

//shuffles the whole deck
void CardDeck::shuffle(){
	cards.shuffle();
}

//returns whether the deck is in order or not
int CardDeck::inOrder(){
	int i;
	for(i=0; i<cards.getSize()-1; i++){
		if(cards[i]>cards[i+1])
			return 0;
	}
	return 1;
}
