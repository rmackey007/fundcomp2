//Ryan Mackey
//driver for the CardDeck class
#include "CardDeck.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(){
	srand(time(NULL));
	CardDeck a(10); // 10 card deck
	if(a.inOrder()){ //check if in order
		cout << a << endl;
		a.shuffle(); // do as requested
		cout << a <<  endl;
	}
	else
		cout << "There's a problem with this deck" << endl;
	return 0;
}
