//Ryan Mackey
//Class declaration for CardDeck
#ifndef CARDDECK_H
#define CARDDECK_H
#include <iostream>
#include <deque>
#include "NDVector.h"

using namespace std;

class CardDeck{

friend ostream& operator<<(ostream& output, CardDeck& deck);

public:
	CardDeck(int n =52); //default to 52 cards
	int getSize();
	void shuffle();
	int inOrder();
	int getCard(int);
private:
	NDVector<int> cards;
};
#endif
