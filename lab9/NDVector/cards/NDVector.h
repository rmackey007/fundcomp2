//Ryan Mackey
//March 23, 2016

#ifndef NDVECTOR_H
#define NDVECTOR_H
#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;

template< typename T >
class NDVector{
public:
	NDVector(int = 10); // done
	NDVector( const NDVector<T> & ); // done
	~NDVector(); // done
	const NDVector &operator=( const NDVector<T> & ); // done
	const T operator[]( int offset)const{ return vector[offset];} // done
	void push_back(T);
	void pop_back();
	int getSize() const; // done
	T back();
	void clear();
	void shuffle();
private:
	T* vector;
	int size;
	int capacity;
};

#endif

template< typename T >
NDVector<T>::NDVector(int vectSize ){ //default constructor for NDVector
	if(vectSize > 0)
		capacity = vectSize;
	else
		capacity = 10;
	vector = new T[ capacity ];
	size = 0;
}

template< typename T > //copy constructor
NDVector<T>::NDVector(const NDVector<T> &NDVectorIQ) : capacity(NDVectorIQ.capacity){
	vector = new T[ capacity ];

	for ( int i = 0; i < capacity; i++)
		vector[i] = NDVectorIQ.vector[i];
}

template< typename T >
void NDVector<T>::shuffle(){
	int temp, num;
	for(int i = 1; i < size; i++){
		num = int(rand()%i);
		temp = vector[i];
		vector[i] = vector[num];
		vector[num] = temp;
	}
}

template< typename T > // deconstructor
NDVector<T>::~NDVector() {
	delete [] vector;
}


template< typename T >
int NDVector<T>::getSize() const {
	return size;
}

template< typename T >
const NDVector<T> &NDVector<T>::operator=( const NDVector<T> &right) {

	if (&right != this ) {

		if( capacity != right.capacity) {
			delete [] vector;
			capacity = right.capacity;
			vector = new T[ capacity ];
		}

		for (int i = 0; i < capacity; i++)
			vector[ i ] = right.vector[ i ];

	}

	return (*this);

}

template< typename T >
void NDVector<T>::pop_back() {
	size--;
	if (size < 0)
		size = 0;
}

template< typename T >
T NDVector<T>::back() {
	return vector[size-1];
}

template< typename T >
void NDVector<T>::clear() {
	size = 0;
}

template< typename T >
void NDVector<T>::push_back(T a) {

	if(size < capacity){
		vector[size] = a;
		size++;
	}
	else{
		T* tempVect;
		tempVect = new T[ capacity * 2 ];
		for(int i; i < size; i++)
			tempVect[i] = vector[i];
		delete [] vector;
		vector = new T[ capacity * 2 ];
		for ( int i; i < size; i ++)
			vector[i] = tempVect[i];
		delete [] tempVect;
		capacity *= 2;
		vector[size] = a;
		size++;
	}
}
