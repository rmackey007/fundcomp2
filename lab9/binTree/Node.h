//Ryan Mackey
//March 28, 2016

#include <iostream>
#include <string>
using namespace std;

#ifndef NODE_H
#define NODE_H

template<typename T > class BinaryTree;

template<typename T >
class Node {

	friend class BinaryTree<T>;

public:
	Node(const T &, string name);
	T getData() const;

private:
	T data;
	string label;
	Node<T> *next;
	Node<T> *left;
	Node<T> *right;
};

template<typename T>
Node<T>::Node(const T &info, string name) : data(info), label(name), next(NULL), left(NULL), right(NULL) {}

template<typename T>
T Node<T>::getData(void) const { return data;}

#endif

