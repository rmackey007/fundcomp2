#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include "Node.h"
#include <iostream>
#include <cmath>

using namespace std;

template<typename T>
class BinaryTree {
public:
	BinaryTree();
	~BinaryTree();
	void appendNode(const T &, std::string label);
	void printLeaves();
	void makeParents(int num);
	void deleteLastNode();
	void inOrder(Node<int> * curNode);
	void preOrder(Node<int> * curNode);
	void postOrder(Node<int> * curNode);
	int search(T item, Node<T> * curNode);
	void finalFour();
	void championship();
	void winner();
	int getSize();
	Node<T> * getFirst();
	bool powerOf2(int n);
private:
	Node<T> *first;
	Node<T> *newNode( const T &, std::string label);
};

template<typename T>
BinaryTree<T>::BinaryTree() :first(NULL){}

template<typename T>
BinaryTree<T>::~BinaryTree(){}

template<typename T>
Node<T> *BinaryTree<T>::newNode(const T &data, std::string label){
	return new Node<T>(data, label);
}

template<typename T>
void BinaryTree<T>::finalFour(){
	Node<T> * walker = first;
	cout << endl << endl << "The final four has come down to..." << endl << endl << walker->label << " vs. ";
	walker = walker->next;
	cout << walker->label << endl << "		&" << endl;
	walker = walker->next;
	cout << walker->label << " vs. ";
	walker = walker->next;
	cout << walker->label << endl << endl << endl << endl << endl;
}

template<typename T>
void BinaryTree<T>::championship(){
	Node<T> * walker = first;
	cout << "As the cookie crumbles, the championship is..." << endl << walker->label << "   VS.   ";
	walker = walker->next;
	cout << walker->label << endl << endl;
}

template<typename T>
void BinaryTree<T>::winner(){
	cout << endl << endl << first->label << " WINS THE STANLEY CUP!!!!!!!!!" << endl;
}

template<typename T>
Node<T> *BinaryTree<T>::getFirst(){
	return first;
}

template<typename T>
void BinaryTree<T>::makeParents(int num){
	if(powerOf2(num)){
		T dataL, dataR, dataW;
		string labelW;
		if(num==2)
			finalFour();
		if(num == 1)
			championship();
		Node<T> *walker = first;
		Node<T> *walker2 = walker;
		Node<T> *holder = NULL;
		Node<T> *holder2 = NULL;
		walker2 = walker2->next;

		//make as many parents as needed
		for(int i = 0; i<num; i++){

			//collect child data
			dataL = walker->data;
			dataR = walker2->data;
			if(dataL>dataR){
				labelW = walker->label;
				dataW = walker->data;
			}
			else{
				labelW = walker2->label;
				dataW = walker2->data;
			}

			//make parent node
			Node<T> *par = newNode(dataW, labelW);
			par->left = walker;
			par->right = walker2;
			holder2 = par;

			if(walker == first){
				first = holder2;
				holder = par;
			}
			else{
				holder->next = holder2;
				holder2->next = NULL;
				holder = par;
			}

			if(walker2->next != NULL){
				walker = walker->next;
				walker = walker->next;
				walker2 = walker->next;
			}
		}
		if(num)
			makeParents(num/2);
		else
			winner();
	}
	else
		cout << "Need 2^n Nodes" << endl;
}

template<typename T>
bool BinaryTree<T>::powerOf2(int n){
	return ((n & (n-1)) == 0);
}

template<typename T>
void BinaryTree<T>::deleteLastNode(){
	Node<T> * walker = first;
	Node<T> * walker2 = walker->next;
	while(walker2->next != NULL){
		walker2 = walker2->next;
		walker = walker->next;
	}
	walker->next = NULL;
}

template<typename T>
void BinaryTree<T>::inOrder(Node<int> * curNode){
	if(powerOf2(getSize())){
		if(curNode != NULL) {
			inOrder(curNode->left);
			cout << curNode->label << endl;
			inOrder(curNode->right);
		}
	}
	else
		cout << "Need 2^n Nodes" << endl;
}

template<typename T>
int BinaryTree<T>::search(T item, Node<T> * curNode){
	if(powerOf2(getSize())){
		if(curNode != NULL && curNode->data == item){
			cout << curNode->label << endl;
			return 1;
		}
		else if(curNode != NULL){
			if(search(item, curNode->left))
				return 1;
			if(search(item, curNode->right))
				return 1;
			return 0;
		}
		return 0;
	}
	else
		cout << "Need 2^n Nodes" << endl;
}

template<typename T>
void BinaryTree<T>::preOrder(Node<int> * curNode){
	if(powerOf2(getSize())){
		if(curNode != NULL) {
			cout << curNode->label << endl;
			preOrder(curNode->left);
			preOrder(curNode->right);
		}
	}
	else
		cout << "Need 2^n Nodes" << endl;
}

template<typename T>
void BinaryTree<T>::postOrder(Node<int> * curNode){
	if(powerOf2(getSize())){
		if(curNode != NULL){
			postOrder(curNode->left);
			postOrder(curNode->right);
			cout << curNode->label << endl;
		}
	}
	else
		cout << "Need 2^n Nodes" << endl;
}

template<typename T>
int BinaryTree<T>::getSize(){
	int size = 1;
	Node<T> *walker = first;
	while(walker->next != NULL){
		walker = walker->next;
		size++;
	}
	return size;
}

template<typename T>
void BinaryTree<T>::appendNode(const T &data, std::string label) {

	Node<T> *nn = newNode(data, label);

	if(!first)
		first = nn;
	else{
		Node<T> *p = first;
		while (p->next != NULL) p = p->next;
		p->next = nn;
	}

	nn->next = NULL;
}

template<typename T>
void BinaryTree<T>::printLeaves(){
	Node<T> *walker = first;
	cout << walker->data << " + " << walker->label << endl;
	while(walker->next != NULL){
		walker = walker->next;
		cout << walker->data << " + " << walker->label << endl;
	}
}
#endif
