#include "BinaryTree.h"
#include <iostream>
#include <fstream>

using namespace std;

int main(){
	BinaryTree< int > tree;
	string name;
	int num;

	ifstream myFile;
	myFile.open("bracket.txt");

	while(!myFile.eof()){
		myFile >> name >> num;
		tree.appendNode(num, name);
	}

	tree.deleteLastNode();
	myFile.close();

//	tree.printLeaves();
	tree.makeParents(tree.getSize()/2);
	cout << endl;

	tree.inOrder(tree.getFirst());
	cout << endl;
	tree.preOrder(tree.getFirst());
	cout << endl;
	tree.postOrder(tree.getFirst());
	cout << endl;

	tree.search(5, tree.getFirst()) || (cout << "NOT PRESENT" << endl);
	tree.search(22, tree.getFirst()) || (cout << "NOT PRESENT" << endl);
	tree.search(21, tree.getFirst()) || (cout << "NOT PRESENT" << endl);
	tree.search(8, tree.getFirst()) || (cout << "NOT PRESENT" << endl);
	return 0;
}
