//Ryan Mackey
//April 13, 2016

#include "Jacc.h"
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <ctype.h>
#include <stdio.h>

using namespace std;

Jacc::Jacc(){
	cout << "File 1 = ";
	cin >> file1;
	cout << "File 2= ";
	cin >> file2;

	un = 0;
	intersect = 0;
}

void Jacc::printWords(){
	cout << "Intersection: " << intersect << " Union: " << un << endl;
	//Getting Jaccard index by dividing intersection by union
	double jaccard = (double)intersect/un;
	cout << "The Jaccard Index of the files is " << jaccard << endl;
}

void Jacc::fill(){
	ifstream f1(file1.c_str());
	ifstream f2(file2.c_str());
	string word;
	char x;
	//all first file words go to union
	while(f1.get(x)){
		if(isspace(x) && word.size()>0){
			if(!words1[word]){
				words1[word] = 1;
				un++;
			}
			word.clear();
		}
		if(isalpha(x)){
			x = tolower(x);
			word += x;
		}
	}
	while(f2.get(x)){
		if(isspace(x) && word.size()>0){
			//if a new file2 word is in file1, intersection increments
			if(words1[word] && !words2[word]){
				intersect++;
				words2[word] = 1;
			}
			//if a new file2 word appears, union increments
			else if(!words1[word] && !words2[word]){
				words2[word] = 1;
				un++;
			}
			word.clear();
		}
		if(isalpha(x)){
			x = tolower(x);
			word += x;
		}
	}
}
