//Ryan Mackey
//April 6, 2016

#ifndef JACC_H
#define JACC_H
#include <iostream>
#include <string>
#include <map>
#include <ctype.h>
#include <fstream>
using namespace std;

class Jacc{
public:
	Jacc();
	void printWords();
	void fill();
private:
	string file1;
	string file2;
	int un;
	int intersect;
	map <string, int> words1;
	map <string, int> words2;
};

#endif
