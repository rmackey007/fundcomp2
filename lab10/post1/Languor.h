//Ryan Mackey
//April 6, 2016

#ifndef LANGUOR_H
#define LANGUOR_H
#include <iostream>
#include <string>
#include <map>
#include <ctype.h>
#include <fstream>
using namespace std;

class Languor{
public:
	Languor();
	void result();
	void fill();
	void langs();
private:
	string file1;
	map <string, int> English;
	map <string, int> French;
};

#endif
