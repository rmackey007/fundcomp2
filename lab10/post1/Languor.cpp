//Ryan Mackey
//April 13, 2016

#include "Languor.h"
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <ctype.h>
#include <stdio.h>

using namespace std;

Languor::Languor(){
	cout << "File= ";
	cin >> file1;
	langs();
}

//seed dictionary
void Languor::langs(){
	English.insert(pair<string, int> ("the", 1));
	English.insert(pair<string, int> ("it", 1));
	English.insert(pair<string, int> ("he", 1));
	English.insert(pair<string, int> ("she", 1));
	English.insert(pair<string, int> ("cookies", 1));
	English.insert(pair<string, int> ("bacon", 1));
	English.insert(pair<string, int> ("ham", 1));
	English.insert(pair<string, int> ("red", 1));
	English.insert(pair<string, int> ("white", 1));
	English.insert(pair<string, int> ("blue", 1));
	English.insert(pair<string, int> ("and", 1));
	English.insert(pair<string, int> ("then", 1));
	English.insert(pair<string, int> ("that", 1));
	English.insert(pair<string, int> ("mine", 1));
	English.insert(pair<string, int> ("his", 1));
	English.insert(pair<string, int> ("of", 1));
	French.insert(pair<string, int>("le", 1));
	French.insert(pair<string, int>("la", 1));
	French.insert(pair<string, int>("notre", 1));
	French.insert(pair<string, int>("et", 1));
	French.insert(pair<string, int>("pierre", 1));
	French.insert(pair<string, int>("je", 1));
	French.insert(pair<string, int>("peux", 1));
	French.insert(pair<string, int>("rouge", 1));
	French.insert(pair<string, int>("bleu", 1));
	French.insert(pair<string, int>("blanc", 1));
	French.insert(pair<string, int>("tricolour", 1));
	French.insert(pair<string, int>("croissant", 1));
	French.insert(pair<string, int>("ton", 1));
	French.insert(pair<string, int>("fait", 1));
	French.insert(pair<string, int>("petit", 1));
	French.insert(pair<string, int>("papillon", 1));
}

void Languor::result(){
	map<string, int>::iterator logos;
	//counting instances of each language
	int US=0, FR=0;
	for(logos = English.begin(); logos!= English.end(); logos++)
		US += logos->second;
	for(logos = French.begin(); logos!= French.end(); logos++)
		FR += logos->second;
	cout << "English: " << US << " French: " << FR << endl;
	if(FR>US)
		cout<<"This file is probably in French" << endl;
	else
		cout<<"This file is probably in English" << endl;
}

void Languor::fill(){
	ifstream f(file1.c_str());
	string word;
	char x;
	map<string, int>::iterator it;
	while(f.get(x)){
		if(isspace(x) && word.size()>0){
			//checking for language presence
			for(it = English.begin(); it!= English.end(); it++){
				if(English[word])
					English[word]++;
			}
			for(it = French.begin(); it!= French.end(); it++){
				if(French[word])
					French[word]++;
			}
			word.clear();
		}
		if(isalpha(x)){
			x = tolower(x);
			word += x;
		}
	}
}
