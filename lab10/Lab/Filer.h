//Ryan Mackey
//April 6, 2016

#ifndef FILER_H
#define FILER_H
#include <iostream>
#include <string>
#include <map>
#include <ctype.h>
#include <fstream>
using namespace std;

class Filer{
public:
	Filer();
	void printWords();
	void fill();
private:
	string file1;
	string file2;
	map <string, int> words1;
	map <string, int> words2;
};

#endif
