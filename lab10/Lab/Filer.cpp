//Ryan Mackey
//April 6, 2016

#include "Filer.h"
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <ctype.h>
#include <stdio.h>

using namespace std;

Filer::Filer(){
	cout << "File 1 = ";
	cin >> file1;
	cout << "File 2 = ";
	cin >> file2;
}

void Filer::printWords(){
	int num = 0;
	//runs through the first file's words, printing name and value
	map<string, int>::iterator myMapIterator;
	for(myMapIterator = words1.begin(); myMapIterator != words1.end(); myMapIterator++){
		cout << myMapIterator->first << " " << myMapIterator->second << endl;
		num++;
	}
	cout << num << " unique words in file 1." << endl;
	num = 0;

	//same for the second
	for(myMapIterator = words2.begin(); myMapIterator != words2.end(); myMapIterator++){
		cout << myMapIterator->first << " " << myMapIterator->second << endl;
		num++;
	}
	cout << num << " unique words in file 2." << endl;
}

//Reads in the words by char, and places them in maps
void Filer::fill(){
	ifstream f1(file1.c_str());
	ifstream f2(file2.c_str());
	string word;
	char x;

	//file one first
	while(f1.get(x)){
		//delimits end of a true word
		if(isspace(x) && word.size()>0){
			if(!words1[word]){
				words1[word] = 1;
			}
			else
				words1[word]++;
			word.clear();
		}

		//concatenation
		if(isalpha(x)){
			x = tolower(x);
			word += x;
		}
	}

	//same for file2
	while(f2.get(x)){
		if(isspace(x) && word.size()>0){
			if(!words2[word]){
				words2[word] = 1;
			}
			else{
				words2[word]++;
			}
			word.clear();
		}
		if(isalpha(x)){
			x = tolower(x);
			word += x;
		}
	}
}
