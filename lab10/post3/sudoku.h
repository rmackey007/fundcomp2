//Ryan Patrick Mackey
//Sudoku class- templated
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <set>

using namespace std;

template< typename T >
class sudoku{
public:
	sudoku();
	void print();
	int solve();
private:
	vector< vector< T > > puzzle;
};

//Templated constructor- fills a 2D vector with a sudoku puzzle form another 
//file
template<typename T >
sudoku<T>::sudoku(){
	vector<T> extraVec;
	T value;
	ifstream infile;
	if(sizeof(T) == sizeof(int)){
		infile.open("sudoku.txt");
	}else if (sizeof(T) == sizeof(char)){
		infile.open("wordoku.txt");
	}
	for(int i=0; i<9; i++){
		for(int j=0; j<9; j++){
			infile >> value;
			extraVec.push_back(value);
		}
		puzzle.push_back(extraVec);
		extraVec.clear();
	}
}

//brute force solve the puzzle
template<typename T >
int sudoku<T>::solve(){
	set<int> poss;
	int again=1;
	for(int i=0; i<9; i++){
		poss.insert(i+1);
	}
	//My use of c++11
	auto iter = poss.begin();
	int mov;

	while(again){
		again=0;
		for(int i = 0; i<9; i++){
			for(int j=0; j<9; j++){
				if(puzzle[i][j]==0){
					//resetting the possibility set
					for(int k=0; k<9; k++){
						poss.insert(k+1);
					}
					for(mov=0; mov<9; mov++){
						//checking for values in poss AND row
						iter = poss.find(puzzle[i][mov]);
						if(iter != poss.end()){
							//shrinking possibilities accordingly
							poss.erase(puzzle[i][mov]);
						}
						iter = poss.find(puzzle[mov][j]);
						if(iter != poss.end()){
							poss.erase(puzzle[mov][j]);
						}
					}
					for(int g1=((i/3)*3); g1<(((i/3)*3)+3); g1++){
						for(int g2=((j/3)*3); g2<(((j/3)*3)+3); g2++){
							//possibility checking again
							iter = poss.find(puzzle[g1][g2]);
							if(iter != poss.end()){
								poss.erase(puzzle[g1][g2]);
							}
						}
					}
					//inserting answer to puzzle
					if(poss.size() == 1){
						puzzle[i][j] = *poss.begin();
						again = 1;
					}
				}
			}
		}
	}
	print();
	return 0;
}

//print the board one block at a time
template<typename T >
void sudoku<T>::print(){
	for(int i=0; i< puzzle.size(); i++){
		for(int j=0; j< puzzle[i].size(); j++){
			cout << puzzle[i][j];
		}
		cout << endl;
	}
	cout << endl;
	return;
}
